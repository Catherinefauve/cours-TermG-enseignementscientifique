def nombre_ouvrages(carte, empruntes, retard):
  if retard != 0:
    return 0
  elif carte == "enfant":
    return 15 - empruntes
  else:
    return 20 - empruntes

print(nombre_ouvrages("enfant", 2, 0))
print(nombre_ouvrages("adulte", 3, 0))
print(nombre_ouvrages("enfant", 4, 2))
