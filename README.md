Ce dépôt contient mes supports de cours, devoirs, etc. pour l'enseignement scientifique, en terminale générale (dans l'enseignement public français). Il ne contient que les sources, en LaTeX. Une version compilée et commentée de ces sources se trouve [sur mon site web](https://lpaternault.frama.io/cours-TermG-enseignementscientifique). Dans mon lycée, le programme de cette discipline est partagé entre les professeurs de mathématiques, sciences physiques et sciences de la vie et de la terre ; ce dépôt ne contient que la partie que j'enseigne (mathématiques et informatique).

Ces documents sont publiés sous license [Creative Commons by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), qui en permet, entre autre, la modification et la réutilisation en classe, gratuitement ([j'explique ici](http://ababsurdo.fr/blog/pourquoi_publier_sous_licence_libre/) mon choix de diffusion libre).

Pour compiler l'ensemble de ces documents, vous pouvez utiliser le logiciel [evariste](http://git.framasoft.org/spalax/evariste) sur le [fichier de configuration](https://git.framasoft.org/lpaternault/cours-TermG-enseignementscientifique/blob/main/evariste.setup) présent à la source du dépôt :

    evariste --verbose evariste.setup

Pour me contacter, voir [la page dédiée](http://ababsurdo.fr/apropos/).
