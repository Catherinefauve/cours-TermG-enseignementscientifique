%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compiler deux fois avec lualatex:
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage[
  a4paper,
  includehead,
  margin=5mm,
]{geometry}

\usepackage{20202021-pablo}
\usepackage{20202021-pablo-paternault}
\usepackage{20202021-pablo-devoir}
\usepackage{20202021-pablo-math}
\usepackage{20202021-pablo-tikz}

\fancyhead[C]{\textbf{2 --- Modèle linéaire}}

\usepackage{fontawesome}
\usepackage{multicol}

\begin{document}

\begin{exemple*}
  % Inspiré du document 1 du manuel d'Enseignement Scientifique de Magnard, 2020.
  La banque mondiale\footnote{\url{https://data.worldbank.org/indicator/SP.POP.TOTL?locations=CN}} fournit des données sur l'évolution de la population chinoise.
  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[xscale=1.2, yscale=1.1, very thick]
        \begin{axis}[
          % Axe x
          axis x line=bottom,
          xlabel={Année},
          xmin=1960, xmax=2020,
          x tick label style={
            /pgf/number format/.cd,
            1000 sep={},
            /tikz/.cd
          },
          % Axe y
          axis y line=left,
          ylabel={Population chinoise (millions)},
          y label style={at={(axis description cs:-0.05,.5)}},
          enlarge y limits,
          y tick label style={
            /pgf/number format/.cd,
            use comma,
            1000 sep={\ },
            /tikz/.cd
          },
          % Quadrillage
          grid=both,
          minor y tick num=1,
        ]
        \addplot[only marks, mark=*] table {
          1960  667.070
          1961  660.330
          1962  665.770
          1963  682.335
          1964  698.355
          1965  715.185
          1966  735.400
          1967  754.550
          1968  774.510
          1969  796.025
          1970  818.315
          1971  841.105
          1972  862.030
          1973  881.940
          1974  900.350
          1975  916.395
          1976  930.685
          1977  943.455
          1978  956.165
          1979  969.005
          1980  981.235
          1981  993.885
          1982 1008.630
          1983 1023.310
          1984 1036.825
          1985 1051.040
          1986 1066.790
          1987 1084.035
          1988 1101.630
          1989 1118.650
          1990 1135.185
          1991 1150.780
          1992 1164.970
          1993 1178.440
          1994 1191.835
          1995 1204.855
          1996 1217.550
          1997 1230.075
          1998 1241.935
          1999 1252.735
          2000 1262.645
          2001 1271.850
          2002 1280.400
          2003 1288.400
          2004 1296.075
          2005 1303.720
          2006 1311.020
          2007 1317.885
          2008 1324.655
          2009 1331.260
          2010 1337.705
          2011 1344.130
          2012 1350.695
          2013 1357.380
          2014 1364.270
          2015 1371.220
          2016 1378.665
          2017 1386.395
          2018 1392.730
          2019 1397.715
        };
        \addplot[domain=.5:1.7] {-6.04*x+11.44};
	\draw (axis cs:1.3,13) node[align=center]{$R^2=\numprint{0.42}$\\$y=-\numprint{6.04}x+\numprint{11.44}$};
        \end{axis}
      \end{tikzpicture}
    \end{center}

    \columnbreak

    \begin{center}
    \begin{tabular}{cS}
      \toprule
      & {Population} \\
      Année & {(millions)} \\
      \midrule
    2010 & 1337,705 \\
    2011 & 1344,130 \\
    2012 & 1350,695 \\
    2013 & 1357,380 \\
    2014 & 1364,270 \\
    2015 & 1371,220 \\
    2016 & 1378,665 \\
    2017 & 1386,395 \\
    2018 & 1392,730 \\
    2019 & 1397,715 \\
    \bottomrule
  \end{tabular}
\end{center}

  \end{multicols}
\end{exemple*}

\begin{definition*}[Suite]
  Une \blanc{suite} $u$ est une liste ordonnée de nombre $u(0)$ ; $u(1)$ ; $u(2)$ ; $u(3)$\ldots

  \emph{\faWarning{} Attention au décalage !} $u( 0)$ est le premier terme, $u(1)$ est le second terme, $u(2)$ est le troisième terme\ldots
\end{definition*}

\begin{exemple}
  On appelle $u(n)$ l'effectif de la population chinoise en millions d'habitants à l'année $2010+n$.

  \begin{enumerate*}[(a)]
    \item Que vaut $u(0)$ ?
    \item Que vaut $u(3)$ ?
    \item Quel est la valeur du cinquième terme de la suite $u$ ?
  \end{enumerate*}
\end{exemple}

Pour une population dont la variation absolue est presque constante d’un palier à l’autre, on peut modéliser son évolution et faire des prédictions en utilisant une suite arithmétique.

\begin{defprop*}[Suite arithmétique]
  Une \blanc{suite arithmétique} est une suite telle que la différence $u(n+1)-u(n)$ est constante. Cette constante est notée $r$ et s’appelle la \blanc{raison} de la suite.

Pour tout entier naturel $n$, on peut prévoir l’effectif d’une population pour une année $n$ a à l’aide de la formule :
\[
  u(n)=u(0)+n\times r \text{ (avec $r =u ( n+1 )-u( n) )$}
\]

Graphiquement, les points de la représentation graphique d'une suite arithmétique (de coordonnées $\left( n;u(n) \right)$) sont \blanc{alignés}.
\end{defprop*}

\begin{exemple}
  \begin{enumerate}[label={(\alph*)}]
    \item Expliquer pourquoi on peut assimiler l'évolution de la population chinoise depuis 1960 à une suite arithmétique.
  \end{enumerate}
On admet que la croissance de la population chinoise à partir de 2010 est modélisée par une suite arithmétique : $u(n)$ est l’effectif de la population chinoise en millions d’habitants à l’année $2010 + n$ et sa raison est $r =7$.
\begin{enumerate}[label={(\alph*)}, resume]
    \item Suivant ce modèle, quel serait l'effectif de la population chinoise en 2022 ? en 2050 ?
  \end{enumerate}
\end{exemple}

\end{document}
